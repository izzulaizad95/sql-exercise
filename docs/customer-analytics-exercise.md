
* 5 real-life SQL projects I would do to learn data transformation *

1 - Summary of customers
A customer table with : first date of purchase, last date of purchase , product most purchased, median number of days between purchases

2 - Cohort analysis.
Cohorts are based on the first month of purchase, and the metrics are: revenue, cumulated revenue , cumulated percentage of customers buying at least 2 times 

3 - Cost of goods sold (COGS)
COGS is based on product purchases (purchase date, units and cost) and product sales (sales date and units). Cost of good sold is calculated at the time of sale = sales units * average cost of products still in inventory at the time of sale

4 - Pareto or ABC analysis.
Segment the products based on 3 categories : category A the top products representing 80% of profit, category B the next 10% of products, and finally C the last 10%.Calculate the % split based on the 3 categories for number of products , units sold and sales value.

5 - RFM analysis
For each customer, calculate its recency (time since last purchase), frequency (number of purchases in the period), and monetary (average order value)

I was inspired by Benjamin Ejzenberg that wrote a post about Python projects. Go follow him for great data analytics and Power BI content (in French)
